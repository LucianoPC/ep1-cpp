/* Pedro Ivo de Andrade - 140086323 */
#include <iostream>
#include "rotina.hpp"
#include <fstream>
#include <sstream>
#include <stdlib.h>


using namespace std;

void leImagem(const char diretorio[], Imagem &imagem){
	char conteudoLinha[100];
	char *aux;
	int linha, coluna, cinza;
	unsigned char *setaValorPixel;

	fstream imgPgm;
	imgPgm.open(diretorio, ios::in | ios::binary);
	//Verifica se a imagem existe no diretorio especificado
	if (!imgPgm){
		cout << "Não foi possível encontrar o Arquivo.\n";
		exit(1);
	}

	//Verifica se o arquivo é do tipo PGM. Olha a primeira linha do arquivo e verifica se é P5 (formato PGM)
	imgPgm.getline(conteudoLinha,100,'\n');
    if ( (conteudoLinha[0]!='P') || (conteudoLinha[1]!='5') ) 
    {   
        cout << "A imagem " << imgPgm;
        cout << " não é do formato PGM\n";
        exit(1);
    }

    //Pula para a segunda linha do arquivo buscando ignorar os comentários existentes.
    imgPgm.getline(conteudoLinha,100,'\n');
    while(conteudoLinha[0]=='#')
        imgPgm.getline(conteudoLinha,100,'\n');

    //Setando as dimensoes da imagem
    linha = strtol(conteudoLinha,&aux,0); //Converte de string para long int
    coluna = atoi(aux); //Converte de char para inteiro

    //Seta os valores em binario (8bits) de cada pixel.
    imgPgm.getline(conteudoLinha,100,'\n');
    cinza=strtol(conteudoLinha,&aux,0); //Converte o valor em binario(char) para inteiro.

    setaValorPixel=(unsigned char *) new unsigned char [linha*coluna];

    //método para verificar se os parametros foram corretamente setados.
    imgPgm.read( reinterpret_cast<char *>(setaValorPixel), (linha*coluna)*sizeof(unsigned char));
	

	if (imgPgm.fail())  //verifica se houve algum erro de resolução da imagem
    {
        cout << "A imagem " << diretorio;
        cout << " possui dimensões incorretas\n";
        exit(1);
    }

    imgPgm.close(); //fecha o arquivo.

    int valor;

    for (int i=0; i<linha;i++){
    	for (int j=0;j<coluna;j++){
    		valor = (int)setaValorPixel[i*coluna+j];
    		imagem.setPixelVal(i,j,valor);
    	}
    }

    delete [] setaValorPixel;

}

void salvarImagem(const char diretorio[], Imagem &imagem){
	int linha, coluna, cinza, valor;
	linha = imagem.getComprimento();
	coluna = imagem.getAltura();
	cinza = imagem.getTonMaxima();

	unsigned char *setaValorPixel;
	ofstream outfile(diretorio);

	setaValorPixel= (unsigned char *) new unsigned char [linha*coluna];

	for (int i=0;i<linha;i++){
		for (int j=0;j<coluna;j++){
			valor = imagem.getPixelVal(i,j);
			setaValorPixel[i*coluna+j] = (unsigned char) valor;
		}
	}

	if (!outfile.is_open()){
		cout << "Impossível abrir arquivo de saída " << diretorio << endl;
	}

	//Começa a escrever o novo arquivo de imagem.
	outfile << "P5\n";
	outfile << linha << " " << coluna << "\n";
	outfile << cinza << "\n";

	outfile.write(reinterpret_cast<char *>(setaValorPixel), (linha*coluna)*sizeof(unsigned char));
	outfile.close();

}

void leCabecalhoParaObjeto(const char diretorio[], int &comprimento, int &altura, int &tonMaxima, bool &ehPGM){
	char conteudoLinha[100], *aux;
	ifstream imgPgm;

	imgPgm.open(diretorio, ios::in | ios::binary);
	if (!imgPgm){
		cout << "Impossivel ler a imagem: " << diretorio << endl;
		exit(1);
	}

	//Começa a ler o cabeçalho do arquivo especificado.
	ehPGM = true; //defindo por padrão.

	imgPgm.getline(conteudoLinha,100,'\n');
	if ((conteudoLinha[0] == 'P') && (conteudoLinha[1] == '5'))
		ehPGM = true;
	else if ((conteudoLinha[0] == 'P') && (conteudoLinha[1] == 54))
		ehPGM = false;
	else{
		cout << "Imagem " << diretorio << " nao eh do tipo PGM ou PPM\n";
		exit(1);
	}

	imgPgm.getline(conteudoLinha,100,'\n');
	while (conteudoLinha[0]=='#')
		imgPgm.getline(conteudoLinha,100,'\n');

	comprimento = strtol(conteudoLinha,&aux,0);
	altura = atoi(aux);

	imgPgm.getline(conteudoLinha,100,'\n');

	tonMaxima = strtol(conteudoLinha,&aux,0);

	imgPgm.close();
}