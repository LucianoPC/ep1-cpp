#include <iostream>
#include "filtrosmooth.hpp"


using namespace std;


void FiltroSmooth::efeito(Imagem &imagem, int division, int size) {
	
		int smooth[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};
		int linhas, colunas, valor;
		linhas = imagem.getComprimento();
		colunas = imagem.getAltura();
		int *m = new int[linhas*colunas];
		
		for (int i=size/2; i < linhas-size/2; i++)
		{
			for (int j = size/2; j < colunas-size/2; j++)
			{
				valor = 0;
				for(int x = -1; x<=1; x++)
				{		
					for(int y = -1; y<=1; y++)
					{		
						valor += smooth[(x+1)+ size*(y+1)] *
					    imagem.getPixelVal(i+x, y+j);						
					}
				}
				
				valor /=division;
				
			
					
				valor= valor < 0 ? 0 : valor;
				valor=valor >255 ? 255 : valor;
				
				m[i+colunas*j] = valor;
			}
		}
		
		for(int i=0;i<linhas; i++)
			for(int j=0; j<colunas; j++)
				imagem.setPixelVal(i, j, m[i+colunas*j]);
				

}
