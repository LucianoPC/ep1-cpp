/* Pedro Ivo de Andrade - 140086323 */
#ifndef _ROTINAS_HPP_
#define _ROTINAS_HPP_


#include "imagem.hpp"

using namespace std;

void leImagem(const char diretorio[], Imagem &imagem);
void salvarImagem(const char diretorio[], Imagem &imagem);
void leCabecalhoParaObjeto(const char diretorio[], int &comprimento, int &altura, int &tonMaxima, bool &ehPGM);

#endif
