#Makefile produzido por Pedro Ivo de Andrade - 14/0086323

#Variavel ue define qual compilador dsera utilizado
CC=g++

#Definindo variaveis para os subdiretorios da pasta
INCLUDE=./include
SRC=./source
OBJ=./obj
BIN=./bin

#Definindo uma flag, uma opção para a compilação do programa.
FLAGS = -Wall

exec: Imagem
	$(CC) -c $(SRC)/main.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(BIN)/exec

Imagem: Rotinas
	$(CC) -c $(SRC)/imagem.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(OBJ)/imagem.o

Rotinas: Filtro
	$(CC) -c $(SRC)/rotina.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(OBJ)/rotina.o

Filtro: FiltroSharpen
	$(CC) -c $(SRC)/filtro.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(OBJ)/filtro.o

FiltroSharpen: FiltroNegativo
	$(CC) -c $(SRC)/filtrosharpen.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(OBJ)/filtro.o

FiltroNegativo: FiltroSmooth
	$(CC) -c $(SRC)/filtronegativo.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(OBJ)/filtro.o

FiltroSmooth:
	$(CC) -c $(SRC)/filtrosmooth.cpp $(FLAGS) -I$(INCLUDE) -o \
	$(OBJ)/filtro.o

clean:
	rm -f $(BIN)/exec $(OBJ)/*o

run:
	cd bin/
	./exec
	
