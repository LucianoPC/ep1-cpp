#include <iostream>
#include "filtronegativo.hpp"


using namespace std;


void FiltroNegativo::efeito(Imagem &imagem) {
		
		int linhas, colunas;
		
		linhas = imagem.getComprimento();
		colunas = imagem.getAltura();
		
		for (int i=0; i < linhas; i++){
			for (int j = 0; j < colunas; j++) {
				imagem.setPixelVal(i,j, 255-imagem.getPixelVal(i, j) );
			}
		} 


}
