#ifndef _FILTRONEGATIVO_HPP_
#define _FILTRONEGATIVO_HPP_

#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"



using namespace std;

class FiltroNegativo : public Filtro {
	public:
		void efeito(Imagem &imagem);	
			
};

#endif
