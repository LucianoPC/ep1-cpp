/* Pedro Ivo de Andrade - 140086323 */
#ifndef _IMAGEM_HPP_
#define _IMAGEM_HPP_



using namespace std;

class Imagem{
private:
	int comprimento;
	int altura;
	int tonMaxima;
	int *pixelVal;

public:
	Imagem();
	Imagem(int comprimento, int altura,int tonMaxima);
	int getComprimento();
	void setComprimento(int comprimento);
	int getAltura();
	void setAltura(int altura);
	int getTonMaxima();
	void setTonMaxima(int tonMaxima);
	int getPixelVal(int comprimento, int altura);
	void setPixelVal(int comprimento, int altura, int valor);

};
#endif