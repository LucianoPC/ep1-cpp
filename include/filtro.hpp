/* Pedro Ivo de Andrade - 140086323 */
#ifndef _FILTRO_HPP_
#define _FILTRO_HPP_

#include <iostream>

using namespace std;

class Filtro {

public:
	int division;
	int size;

public:
	Filtro();
	Filtro(int division, int size);
	int getDivision();
	void setDivision(int division);
	int getSize();
	void setSize(int size);
};

#endif