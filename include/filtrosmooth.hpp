#ifndef _FILTROSMOOTH_H_
#define _FILTROSMOOTH_H_

#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"

using namespace std;

class FiltroSmooth : public Filtro{
	public:

		void efeito(Imagem &imagem,int division,int size);

};

#endif
