#ifndef _FILTROSHARPEN_H_
#define _FILTROSHARPEN_H_

#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"


using namespace std;

class FiltroSharpen : public Filtro{
	public:
		void efeito(Imagem &imagem,int div, int sz);

};

#endif
