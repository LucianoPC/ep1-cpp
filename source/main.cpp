/* Pedro Ivo de Andrade - 140086323 */
#include <iostream>

#include "imagem.hpp"
#include "rotina.hpp"
#include "filtro.hpp"
#include "filtronegativo.hpp"
#include "filtrosharpen.hpp"
#include "filtrosmooth.hpp"

using namespace std;

int main () {
	int linhas, colunas, cinzas;
	bool ehPGM;
	
	leCabecalhoParaObjeto("../lena512.pgm",linhas,colunas,cinzas,ehPGM);
	Imagem imagemLena(linhas,colunas,cinzas);
	
	leImagem("../lena512.pgm",imagemLena);

	int opcao;

	FiltroNegativo negativo;
	FiltroSharpen sharpen;
	FiltroSmooth smooth;

	do{
		cout << "#####\t\tEscolha o Filtro\t\t#####\n";
		cout << "[1].Negativo\n"
			 << "[2].Sharpen\n"
			 << "[3].Smooth\n"
			 << "[0].Sair\n";

		cin >> opcao;

		switch(opcao){
			case 1:
				negativo.efeito(imagemLena);
				salvarImagem("../Negativo.pgm",imagemLena);
				break;
			case 2:
				sharpen.setDivision(1);
				sharpen.setSize(3);
				sharpen.efeito(imagemLena, sharpen.getDivision(), sharpen.getSize());
				salvarImagem("../Sharpen.pgm",imagemLena);
				break;
			case 3:
				smooth.setDivision(9);
				smooth.setSize(3);
				smooth.efeito(imagemLena, smooth.getDivision(), smooth.getSize());
				salvarImagem("../Smooth",imagemLena);
				break;
			default:
				cout << "Opcao Incorreta\n\n";
				break;
		}
	}while(opcao!=0);

	return 0;
}