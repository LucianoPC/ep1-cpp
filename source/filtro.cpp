#include "filtro.hpp"

using namespace std;

Filtro::Filtro(){
	this->division = 0;
	this->size = 0;
}

Filtro::Filtro(int division, int size){
	this->division = division;
	this->size = size;
}

int Filtro::getDivision(){
	
	return division;

}
void Filtro::setDivision(int division){
	
	this->division = division;

}

int Filtro::getSize(){
	
	return size;

}
void Filtro::setSize(int size){
	
	this->size = size;

}
