/* Pedro Ivo de Andrade - 140086323 */
#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>

//Método construtor para uma imagem [0][0]
Imagem::Imagem(){
	this->comprimento=0;
	this->altura=0;
	this->tonMaxima=0;
	pixelVal=NULL;
}

//Controi a imagem com os parametros definidos. Armazena 0 na tonalidade de cada pixel
Imagem::Imagem(int comprimento, int altura, int tonMaxima){
	this->comprimento=comprimento;
	this->altura=altura;
	this->tonMaxima=tonMaxima;

	pixelVal = new int [this->comprimento*this->altura];
}

int Imagem::getComprimento(){
	return comprimento;
}
void Imagem::setComprimento(int comprimento){
	this->comprimento=comprimento;
}

int Imagem::getAltura(){
	return altura;
}
void Imagem::setAltura(int altura){
	this->altura=altura;
}

int Imagem::getTonMaxima(){
	return tonMaxima;
}
void Imagem::setTonMaxima(int tonMaxima){
	this->tonMaxima=tonMaxima;
}

//Mostra a tonalidade de cinza de um pixel
int Imagem::getPixelVal(int comprimento, int altura){
	int coluna = getAltura();
	return pixelVal[comprimento*coluna+altura];
}
//Seta um tom de cinza de um pixel
void Imagem::setPixelVal(int comprimento, int altura, int valor){
	int coluna = getAltura();
	pixelVal[comprimento*coluna+altura] = valor;
}


